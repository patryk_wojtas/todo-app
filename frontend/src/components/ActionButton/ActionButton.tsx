import React from 'react'
import './ActionButton.scss'

interface PropsInterface {
  onClick?: (...args: any) => any
  text: string,
  type?: 'button' | 'submit' | 'reset'
}

const ActionButton = ({ onClick, text, type = 'button' }: PropsInterface) => {
  return (
    <button type={type} className='action-button' onClick={onClick}>{text}</button>
  )
}

export default ActionButton
