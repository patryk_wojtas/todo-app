import React, { Dispatch, SetStateAction, useState } from 'react'
import { TaskInterface } from '../../model/TaskInterface'
import './TaskDetailsModal.scss'
import formatDate from '../../helper/formatDate'
import { useTranslation } from 'react-i18next'
import updateTask from '../../helper/apiCommunicationMethods/updateTask'
import { useDispatch, useSelector } from 'react-redux'
import { setTasksList } from '../../store/actions/tasksListActions'
import sortTasks from '../../helper/sortTasks'
import { StateInterface } from '../../model/StateInterface'
import ActionButton from '../ActionButton/ActionButton'
import deleteTask from '../../helper/apiCommunicationMethods/deleteTask'

interface PropsInterface {
  task: TaskInterface
  setShowModal: Dispatch<SetStateAction<Boolean>>
}

const TaskDetailsModal = ({ task, setShowModal }: PropsInterface) => {
  const [taskName, setTaskName] = useState(task.name)
  const [taskDescription, setTaskDescription] = useState(task.description)
  const [taskNameEditing, setTaskNameEditing] = useState(false)
  const [taskDescriptionEditing, setTaskDescriptionEditing] = useState(false)
  const sorting = useSelector((state: StateInterface) => state.sorting)
  const { t } = useTranslation()
  const dispatch = useDispatch()

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement|HTMLTextAreaElement>) => {
    if (event.target.getAttribute('name') === 'name') {
      setTaskName(event.target.value)
    } else {
      setTaskDescription(event.target.value)
    }
  }

  const handleUpdateData = async () => {
    setTaskNameEditing(false)
    setTaskDescriptionEditing(false)
    if (taskName !== task.name || taskDescription !== task.description) {
      const updatedTasksList = await updateTask({
        ...task,
        name: taskName,
        description: taskDescription
      })

      dispatch(setTasksList(sortTasks(updatedTasksList, sorting)))
    }
  }

  const handleEndEditing = (event: any) => {
    if (!event.target.classList.contains('task-details__editable') && (taskNameEditing || taskDescriptionEditing)) {
      handleUpdateData()
    }
  }

  const handleFinishTask = async () => {
    const updatedTasksList: TaskInterface[] = await updateTask({
      ...task,
      finishDate: task.finishDate ? null : new Date()
    })

    dispatch(setTasksList(sortTasks(updatedTasksList, sorting)))
  }

  const handleDeleteTask = async () => {
    const updatedTasksList: TaskInterface[] = await deleteTask(task.order)
    dispatch(setTasksList(sortTasks(updatedTasksList, sorting)))
    setShowModal(false)
  }

  return (
    <div className='task-details' onClick={handleEndEditing}>
      {taskNameEditing ? (
        <input
          className='task-details__input task-details__editable'
          type='text'
          name='name'
          value={taskName}
          onBlur={handleUpdateData}
          onKeyDown={(event: React.KeyboardEvent) => {
            if (event.key === 'Enter') {
              handleUpdateData()
            }
          }}
          onChange={handleInputChange}
        />
      ) : <h3 className='task-details__name' onClick={() => setTaskNameEditing(true)}>{task.name}</h3>}
      <p className='task-details__date'>
        {task.finishDate ? t('finished') + ' ' + formatDate(task.finishDate) : t('not-finished')}
      </p>
      {taskDescriptionEditing ? (
        <textarea
          name='description'
          className='task-details__input task-details__input--textarea task-details__editable'
          onChange={handleInputChange}
          onBlur={handleUpdateData}
          value={taskDescription}
        />
      ) : (
        <p className='task-details__description' onClick={() => setTaskDescriptionEditing(true)}>{task.description}</p>
      )}
      <div className='task-details__actions'>
        <ActionButton text={task.finishDate ? t('start') : t('finish')} onClick={handleFinishTask} />
        <ActionButton text={t('delete')} onClick={handleDeleteTask} />
      </div>
    </div>
  )
}

export default TaskDetailsModal
