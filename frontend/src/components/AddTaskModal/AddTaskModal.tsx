import React, { Dispatch, SetStateAction } from 'react'
import { Field, Form, Formik, FormikValues } from 'formik'
import { useTranslation } from 'react-i18next'
import ActionButton from '../ActionButton/ActionButton'
import './AddTaskModal.scss'
import { useDispatch, useSelector } from 'react-redux'
import { StateInterface } from '../../model/StateInterface'
import findHighestOrder from '../../helper/findHighestOrder'
import addTask from '../../helper/apiCommunicationMethods/addTask'
import { TaskInterface } from '../../model/TaskInterface'
import { setTasksList } from '../../store/actions/tasksListActions'
import { Option } from 'react-dropdown'
import sortTasks from '../../helper/sortTasks'

interface PropsInterface {
  setShouldShowModal: Dispatch<SetStateAction<Boolean>>
}

const AddTaskModal = ({ setShouldShowModal }: PropsInterface) => {
  const { t } = useTranslation()
  const tasks: TaskInterface[] = useSelector((state: StateInterface) => state.tasksList)
  const sorting: Option = useSelector((state: StateInterface) => state.sorting)
  const dispatch = useDispatch()

  const onAddTask = async (values: FormikValues) => {
    const newTask: TaskInterface = {
      name: values.name,
      description: values.description,
      order: findHighestOrder(tasks).order + 1
    }

    const updatedList: TaskInterface[] = await addTask(newTask)
    dispatch(setTasksList(sortTasks(updatedList, sorting)))
    setShouldShowModal(false)
  }

  return (
    <Formik
      initialValues={{
        name: '',
        description: ''
      }}
      onSubmit={(values: FormikValues) => {
        onAddTask(values)
      }}
    >
      <Form className='add-task'>
        <Field
          type='text'
          name='name'
          placeholder={t('task-name')}
          className='add-task__field'
        />
        <Field
          component='textarea'
          name='description'
          placeholder={t('task-description')}
          className='add-task__field add-task__field--textarea'
        />
        <ActionButton
          type='submit'
          text={t('add-task')}
        />
      </Form>
    </Formik>
  )
}

export default AddTaskModal
