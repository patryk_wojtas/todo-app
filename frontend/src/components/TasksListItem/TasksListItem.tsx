import React, { useState } from 'react'
import './TasksListItem.scss'
import { TaskInterface } from '../../model/TaskInterface'
import { useTranslation } from 'react-i18next'
import ActionButton from '../ActionButton/ActionButton'
import deleteTask from '../../helper/apiCommunicationMethods/deleteTask'
import { useDispatch, useSelector } from 'react-redux'
import { setTasksList } from '../../store/actions/tasksListActions'
import updateTask from '../../helper/apiCommunicationMethods/updateTask'
import formatDate from '../../helper/formatDate'
import PortalModal from '../PortalModal/PortalModal'
import TaskDetailsModal from '../TaskDetailsModal/TaskDetailsModal'
import sortTasks from '../../helper/sortTasks'
import { StateInterface } from '../../model/StateInterface'
import { Option } from 'react-dropdown'
import { SortableElement } from 'react-sortable-hoc'

interface PropsInterface {
  task: TaskInterface
}

const TasksListItem = SortableElement(({ task }: PropsInterface) => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const [shouldShowModal, setShouldShowModal] = useState(false)
  const sorting: Option = useSelector((state: StateInterface) => state.sorting)

  const onTaskDelete = async (orderNumber: number) => {
    const updatedTasksList: TaskInterface[] = await deleteTask(orderNumber)
    dispatch(setTasksList(sortTasks(updatedTasksList, sorting)))
  }

  const onTaskFinish = async (task: TaskInterface) => {
    const updatedTasksList: TaskInterface[] = await updateTask({
      ...task,
      finishDate: task.finishDate ? null : new Date()
    })

    dispatch(setTasksList(sortTasks(updatedTasksList, sorting)))
  }

  return (
    <div className='tasks-list-item'>
      <div className='tasks-list-item__data' onClick={() => setShouldShowModal(true)}>
        <h3 className='tasks-list-item__name'>{task.name}</h3>
        <p className='tasks-list-item__finish-date'>
          {task.finishDate ? t('finished') + ' ' + formatDate(task.finishDate) : t('not-finished')}
        </p>
      </div>
      {
        shouldShowModal && (
          <PortalModal setShouldShowModal={setShouldShowModal}>
            <TaskDetailsModal task={task} setShowModal={setShouldShowModal} />
          </PortalModal>
        )
      }
      <div className='tasks-list-item__actions'>
        <ActionButton onClick={() => { onTaskDelete(task.order) }} text={t('delete')} />
        <ActionButton onClick={() => { onTaskFinish(task) }} text={task.finishDate ? t('start') : t('finish')} />
      </div>
    </div>
  )
})

export default TasksListItem
