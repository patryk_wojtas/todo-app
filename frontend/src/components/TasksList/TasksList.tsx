import React from 'react'
import './TasksList.scss'
import TopPanel from '../TopPanel/TopPanel'
import useInitData from '../../hooks/useInitData'
import { useDispatch, useSelector } from 'react-redux'
import { StateInterface } from '../../model/StateInterface'
import { TaskInterface } from '../../model/TaskInterface'
import SortableTasksList from '../SortableTasksList/SortableTasksList'
import { setTasksList } from '../../store/actions/tasksListActions'
import sortTasks from '../../helper/sortTasks'
import updateTasksList from '../../helper/apiCommunicationMethods/updateTasksList'
import { Option } from 'react-dropdown'

const TasksList = () => {
  useInitData()
  const tasks: TaskInterface[] = useSelector((state: StateInterface) => state.tasksList)
  const dispatch = useDispatch()
  const sorting: Option = useSelector((state: StateInterface) => state.sorting)

  const handleSortItems = async (sortData: any) => {
    const oldIndex = sortData.oldIndex
    const newIndex = sortData.newIndex

    const oldTaskOrderOnNewIndexPosition = tasks[newIndex].order
    const taskWithUpdatedOrderNumber: TaskInterface = {
      ...tasks[oldIndex],
      order: oldTaskOrderOnNewIndexPosition
    }

    const updatedTasksList: TaskInterface[] = await updateTasksList(taskWithUpdatedOrderNumber, oldTaskOrderOnNewIndexPosition)
    dispatch(setTasksList(sortTasks(updatedTasksList, sorting)))
  }

  return tasks ? (
    <div className='tasks-list'>
      <div className='tasks-list__container'>
        <div className='tasks-list__top-panel'>
          <TopPanel />
        </div>
        {/* @ts-ignore */}
        <SortableTasksList items={(tasks as any)} onSortEnd={handleSortItems} lockAxis='y' />
      </div>
    </div>
  ) : (
    <div>Loading....</div>
  )
}

export default TasksList
