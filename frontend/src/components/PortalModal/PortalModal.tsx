import React, { Dispatch, SetStateAction } from 'react'
import ReactDOM from 'react-dom'
import './PortalModal.scss'

interface PropsInterface {
  children: React.ReactNode,
  setShouldShowModal: Dispatch<SetStateAction<Boolean>>
}

const modalRoot = document.getElementById('portal-root')

const PortalModal = ({ children, setShouldShowModal }: PropsInterface) => {
  return ReactDOM.createPortal(
    <div className='modal'>
      <div className='modal__overlay' onClick={() => setShouldShowModal(false)} />
      <div className='modal__wrapper'>
        <div className='modal__exit-button' onClick={() => setShouldShowModal(false)} />
        {children}
      </div>
    </div>
    , modalRoot
  )
}

export default PortalModal
