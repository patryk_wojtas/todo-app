import React from 'react'
import { SortableContainer } from 'react-sortable-hoc'
import { TaskInterface } from '../../model/TaskInterface'
import TasksListItem from '../TasksListItem/TasksListItem'

interface PropsInterface {
  items: any
  onSortEnd: any
}

const SortableTasksList = SortableContainer(({ items }: PropsInterface) => {
  return (
    <div className='tasks-list__wrapper'>
      {items.map((task: TaskInterface, index: number) => (
        <TasksListItem key={task.order} task={task} index={index} />
      ))}
    </div>
  )
})

export default SortableTasksList
