import React, { useState } from 'react'
import ActionButton from '../ActionButton/ActionButton'
import Dropdown, { Option } from 'react-dropdown'
import sortingOptions from '../../config/sortingOptions'
import './TopPanel.scss'
import { useTranslation } from 'react-i18next'
import PortalModal from '../PortalModal/PortalModal'
import AddTaskModal from '../AddTaskModal/AddTaskModal'
import { useDispatch, useSelector } from 'react-redux'
import { setSorting } from '../../store/actions/sortingActions'

const TopPanel = () => {
  const [shouldShowAddTaskModal, setShouldShowAddTaskModal] = useState(false)
  const { t } = useTranslation()
  const dispatch = useDispatch()

  const onSortingChange = (option: Option) => {
    dispatch(setSorting(option))
  }

  const onAddTask = () => {
    setShouldShowAddTaskModal(true)
  }

  return (
    <>
      <div className='top-panel__dropdown-wrapper'>
        {t('sort-by')}
        <Dropdown
          options={sortingOptions}
          value={sortingOptions.find((option) => option.value === 'name|asc')}
          onChange={onSortingChange}
          className='top-panel__dropdown'
          controlClassName='top-panel__dropdown-control'
          placeholderClassName='top-panel__dropdown-placeholder'
          menuClassName='top-panel__dropdown-menu'

        />
      </div>
      <ActionButton text='Add Task' onClick={onAddTask} />
      {
        shouldShowAddTaskModal && (
          <PortalModal setShouldShowModal={setShouldShowAddTaskModal}>
            <AddTaskModal setShouldShowModal={setShouldShowAddTaskModal} />
          </PortalModal>
        )

      }
    </>
  )
}

export default TopPanel
