import { Option } from 'react-dropdown'

const sortingOptions: Option[] = [
  {
    value: 'name|asc',
    label: 'Nazwa A-Z'
  }, {
    value: 'name|desc',
    label: 'Nazwa Z-A'
  }, {
    value: 'finishDate|asc',
    label: 'Data A-Z'
  }, {
    value: 'finishDate|desc',
    label: 'Data Z-A'
  }
]

export const defaultSorting = sortingOptions[0]

export default sortingOptions
