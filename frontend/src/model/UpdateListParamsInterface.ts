import { TaskInterface } from './TaskInterface'

export interface UpdateListParamsInterface {
  taskToMove: TaskInterface,
  oldTaskOrder: number
}
