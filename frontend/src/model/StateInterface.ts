import { TaskInterface } from './TaskInterface'
import { Option } from 'react-dropdown'

export interface StateInterface {
  tasksList: TaskInterface[]
  sorting: Option
}
