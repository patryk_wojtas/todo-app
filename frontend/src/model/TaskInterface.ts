export interface TaskInterface {
  name: string,
  description: string,
  finishDate?: Date|null
  order: number
}
