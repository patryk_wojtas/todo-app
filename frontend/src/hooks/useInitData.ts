import { useContext, useEffect } from 'react'
import getTasksList from '../helper/apiCommunicationMethods/getTasksList'
import { useDispatch, useSelector } from 'react-redux'
import { setTasksList } from '../store/actions/tasksListActions'
import { TaskInterface } from '../model/TaskInterface'
import sortTasks from '../helper/sortTasks'
import { StateInterface } from '../model/StateInterface'

const useInitData = () => {
  const dispatch = useDispatch()
  const sorting = useSelector((state: StateInterface) => state.sorting)

  useEffect(() => {
    (async () => {
      const tasks: TaskInterface[] = await getTasksList()
      dispatch(setTasksList(sortTasks(tasks, sorting)))
    })()
  }, [sorting])
}

export default useInitData
