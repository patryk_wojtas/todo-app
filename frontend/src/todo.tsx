import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import i18n from 'i18next'
import { initReactI18next, I18nextProvider } from 'react-i18next'
import resources from './translations/resources'
import reducerRoot from './store/reducers'
import TasksList from './components/TasksList/TasksList'
import './index.scss'

if (document.getElementById('root')) {
  const store = createStore(reducerRoot, applyMiddleware(thunk))

  const i18nInstance = i18n.createInstance()
  i18nInstance
    .use(initReactI18next)
    .init({
      resources,
      lng: 'pl',
      keySeparator: false,
      interpolation: {
        escapeValue: false
      }
    })

  ReactDOM.render(
    <Provider store={store}>
      <I18nextProvider i18n={i18nInstance}>
        <TasksList />
      </I18nextProvider>
    </Provider>,
    document.getElementById('root')
  )
}
