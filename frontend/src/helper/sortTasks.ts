import { TaskInterface } from '../model/TaskInterface'
import { Option } from 'react-dropdown'

export default (tasks: TaskInterface[], sortBy: Option): TaskInterface[] => {
  const sortingVariable: string = sortBy.value.split('|')[0]
  const sortingOrder: string = sortBy.value.split('|')[1]

  const tasksSorted = tasks.sort((a: any, b: any): number => {
    if (sortingVariable === 'name') {
      return a[sortingVariable].toLowerCase().localeCompare(b[sortingVariable].toLowerCase())
    } else if (sortingVariable === 'finishDate') {
      return new Date(a[sortingVariable]).getTime() - new Date(b[sortingVariable]).getTime()
    }
  })

  if (sortingOrder === 'desc') {
    return tasksSorted.reverse()
  }

  return tasksSorted
}
