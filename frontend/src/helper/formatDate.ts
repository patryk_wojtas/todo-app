export default (date: Date) => {
  const dateObj = new Date(date)
  return `${dateObj.getDay()}.${dateObj.getMonth()}.${dateObj.getFullYear()} ${dateObj.getHours()}:${dateObj.getMinutes()}`
}
