import { TaskInterface } from '../model/TaskInterface'

export default (tasks: TaskInterface[]) => {
  return tasks.reduce((prev, curr) => prev.order < curr.order ? curr : prev, tasks[0])
}
