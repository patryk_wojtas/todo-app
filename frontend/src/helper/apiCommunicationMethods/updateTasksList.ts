import { TaskInterface } from '../../model/TaskInterface'
import axios, { AxiosError, AxiosResponse } from 'axios'
import { apiUrl } from '../../config/baseConfig'
import getTasksList from './getTasksList'

export default async (taskWithNewOrder: TaskInterface, oldOrderNumber: number): Promise<TaskInterface[]> => {
  const updated = await axios.patch(apiUrl + 'tasks', {
    newTask: taskWithNewOrder,
    oldOrderNumber
  })
    .then((response: AxiosResponse) => response.data)
    .catch((error: AxiosError) => error)

  return await getTasksList()
}
