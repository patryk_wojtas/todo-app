import axios, { AxiosError, AxiosResponse } from 'axios'
import { apiUrl } from '../../config/baseConfig'
import getTasksList from './getTasksList'
import { TaskInterface } from '../../model/TaskInterface'

export default async (orderNumber: number): Promise<TaskInterface[]> => {
  const deleted = await axios.delete(apiUrl + 'tasks/' + orderNumber)
    .then((response: AxiosResponse) => response.data)
    .catch((error: AxiosError) => error)

  return await getTasksList()
}
