import axios, { AxiosError, AxiosResponse } from 'axios'
import { apiUrl } from '../../config/baseConfig'
import { TaskInterface } from '../../model/TaskInterface'
import getTasksList from './getTasksList'

export default async (task: TaskInterface): Promise<TaskInterface[]> => {
  await axios.post(apiUrl + 'tasks', task)
    .then((response: AxiosResponse) => response.data)
    .catch((error: AxiosError) => error)

  return await getTasksList()
}
