import { TaskInterface } from '../../model/TaskInterface'
import axios, { AxiosError, AxiosResponse } from 'axios'
import { apiUrl } from '../../config/baseConfig'
import getTasksList from './getTasksList'

export default async (newTaskData: TaskInterface): Promise<TaskInterface[]> => {
  const updated = await axios.patch(apiUrl + 'tasks/' + newTaskData.order, newTaskData)
    .then((response: AxiosResponse) => response.data)
    .catch((error: AxiosError) => error)

  return await getTasksList()
}
