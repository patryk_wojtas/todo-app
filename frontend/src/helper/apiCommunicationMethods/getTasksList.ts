import axios, { AxiosError, AxiosResponse } from 'axios'
import { apiUrl } from '../../config/baseConfig'
import { TaskInterface } from '../../model/TaskInterface'

export default async (): Promise<TaskInterface[]> => {
  return await axios.get(apiUrl + 'tasks')
    .then((response: AxiosResponse) => response.data)
    .catch((error: AxiosError) => error)
}
