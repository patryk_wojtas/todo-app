const resources = {
  pl: {
    translation: {
      'sort-by': 'Sortuj',
      'not-finished': 'W toku..',
      delete: 'Usuń',
      finish: 'Zakończ',
      finished: 'Zakończono',
      start: 'Rozpocznij',
      'task-name': 'Tytuł zadania..',
      'task-description': 'Treść zadania..',
      'add-task': 'Dodaj zadanie'
    }
  }
}

export default resources
