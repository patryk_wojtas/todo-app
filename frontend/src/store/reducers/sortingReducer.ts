import { SET_SORTING, SetSortingInterface } from '../actions/actionsType'
import { Option } from 'react-dropdown'
import { defaultSorting } from '../../config/sortingOptions'

export function sortingReducer (state: Option = defaultSorting, action: SetSortingInterface) {
  switch (action.type) {
    case SET_SORTING:
      state = action.payload
      return state
    default:
      return state
  }
}

export default sortingReducer
