import { combineReducers } from 'redux'
import { tasksListReducer } from './tasksListReducer'
import sortingReducer from './sortingReducer'

export default combineReducers({
  tasksList: tasksListReducer,
  sorting: sortingReducer
})
