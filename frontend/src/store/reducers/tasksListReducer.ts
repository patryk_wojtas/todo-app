import { SET_TASKS_LIST, TaskListActions } from '../actions/actionsType'
import { TaskInterface } from '../../model/TaskInterface'

export function tasksListReducer (state: TaskInterface[]|null = null, action: TaskListActions) {
  switch (action.type) {
    case SET_TASKS_LIST:
      state = action.payload
      return state
    default:
      return state
  }
}
