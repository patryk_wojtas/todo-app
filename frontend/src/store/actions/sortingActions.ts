import { Option } from 'react-dropdown'
import { SET_SORTING, SetSortingInterface } from './actionsType'

export const setSorting = (payload: Option): SetSortingInterface => {
  return {
    type: SET_SORTING,
    payload
  }
}
