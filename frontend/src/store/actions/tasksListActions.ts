import { TaskInterface } from '../../model/TaskInterface'
import { SET_TASKS_LIST, SetTasksListInterface } from './actionsType'

export const setTasksList = (payload: TaskInterface[]): SetTasksListInterface => {
  return {
    type: SET_TASKS_LIST,
    payload
  }
}
