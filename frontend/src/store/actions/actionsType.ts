import { TaskInterface } from '../../model/TaskInterface'
import { Option } from 'react-dropdown'

export interface SetTasksListInterface {
  type: string,
  payload: TaskInterface[]
}

export interface SetSortingInterface {
  type: string,
  payload: Option
}

export type TaskListActions = SetTasksListInterface

export const SET_TASKS_LIST = 'SET_TASKS_LIST'
export const SET_SORTING = 'SET_SORTING'
