import mongoose from 'mongoose'
import config from '../config/config'

const database = () => {
  return mongoose.connect(`mongodb+srv://${config.DB_USER}:${config.DB_PASSWORD}@todo.jfbks.mongodb.net/${config.DB_NAME}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(() => {
    console.log('db_connected')
  }).catch(() => {
    console.error('db_connection_error')
  })
}

export default database
