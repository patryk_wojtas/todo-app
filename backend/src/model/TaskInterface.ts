/* eslint no-undef: "off" */

import { Document } from 'mongoose'

interface TaskInterface extends Document {
  name: string,
  description: string,
  finishDate: Date|null
  order: number
}

export default TaskInterface
