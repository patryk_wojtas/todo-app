import { NextFunction, Request, Response } from 'express'
import database from '../database/mongoose'
import Task from '../schema/Task'
import mongoose from 'mongoose'
import TaskInterface from '../model/TaskInterface'

const getToDoList = async (req: Request, res: Response, next: NextFunction) => {
  await database()

  await Task.find().sort('order').exec()
    .then((response: TaskInterface[]) => {
      res.status(200).json(response)
    })
    .catch(() => {
      res.status(500).send({
        message: 'Cannot find data in database'
      })
    })
    .finally(() => {
      mongoose.connection.close()
    })
}

const updateToDoList = async (req: Request, res: Response, next: NextFunction) => {
  await database()
  let itemsToUpdate: TaskInterface[]

  const taskWithNewOrder: TaskInterface = req.body.newTask
  const oldOrderNumber: number = req.body.oldOrderNumber
  const movedForward: boolean = oldOrderNumber < taskWithNewOrder.order

  await Task.deleteOne({ _id: taskWithNewOrder._id })

  if (!movedForward) {
    itemsToUpdate = await Task.find({
      order: { $gte: taskWithNewOrder.order }
    }).sort('order').exec()
  } else {
    itemsToUpdate = await Task.find({
      order: { $lte: taskWithNewOrder.order, $gte: oldOrderNumber }
    }).sort('order').exec()
  }

  const reversedItems: TaskInterface[] = movedForward ? itemsToUpdate : itemsToUpdate.reverse()
  reversedItems.forEach(async (item) => {
    await Task.findByIdAndUpdate(item._id, {
      $inc: { order: movedForward ? -1 : 1 }
    })
  })

  const updatedTask: TaskInterface = new Task({
    name: taskWithNewOrder.name,
    description: taskWithNewOrder.description,
    order: taskWithNewOrder.order
  })

  await updatedTask.save() // BUT I DON'T HAVE IDEA HOW TO MOVE THIS ITEM TO PROPER POSITION, BUT IT DOESN'T MATTER - IT WORKS PROPERLY
  res.status(200).send({
    message: 'success'
  })
}

const addTaskToList = async (req: Request, res: Response, next: NextFunction) => {
  await database()

  const taskData: TaskInterface = req.body

  const task = new Task({
    name: taskData.name,
    description: taskData.description,
    order: taskData.order
  })

  await task.save()
    .then(() => {
      res.status(201).send({
        message: 'created'
      })
    })
    .catch(() => {
      res.status(500).send({
        message: 'error with saving data into database'
      })
    })
    .finally(() => {
      mongoose.connection.close()
    })
}

const getToDoTask = async (req: Request, res: Response, next: NextFunction) => {
  await database()
  const taskNumber: number = Number(req.params.index)

  await Task.findOne({
    order: taskNumber
  }).exec()
    .then((response: TaskInterface) => {
      res.status(200).json(response)
    })
    .catch(() => {
      res.status(500).send({
        message: 'Cannot find data in database'
      })
    })
    .finally(() => {
      mongoose.connection.close()
    })
}

const updateToDoTask = async (req: Request, res: Response, next: NextFunction) => {
  await database()

  const newTaskData: TaskInterface = req.body

  await Task.updateOne({ order: newTaskData.order }, {
    ...newTaskData
  })
    .then(() => {
      res.status(200).send({ message: 'success' })
    })
    .catch(() => {
      res.status(500).send({
        message: 'Cannot update task'
      })
    })
    .finally(() => {
      mongoose.connection.close()
    })
}

const removeToDoTask = async (req: Request, res: Response, next: NextFunction) => {
  await database()

  const taskNumber: number = Number(req.params.index)

  await Task.deleteOne({ order: taskNumber })

  const itemsToUpdate: TaskInterface[] = await Task.find({
    order: { $gt: taskNumber }
  }).sort('order').exec()

  itemsToUpdate.forEach(async (item: TaskInterface) => {
    await Task.findByIdAndUpdate(item._id, {
      $inc: { order: -1 }
    })
  })

  res.status(200).send({
    message: 'success'
  })
}

export {
  getToDoList,
  updateToDoList,
  addTaskToList,
  getToDoTask,
  updateToDoTask,
  removeToDoTask
}
