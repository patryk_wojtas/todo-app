import express, { Router } from 'express'
import {
  removeToDoTask,
  updateToDoTask,
  getToDoTask,
  addTaskToList,
  updateToDoList,
  getToDoList
} from '../controller/toDoController'

const router: Router = express.Router()

router.get('/tasks', getToDoList)
router.patch('/tasks', updateToDoList)
router.post('/tasks', addTaskToList)

router.get('/tasks/:index', getToDoTask)
router.patch('/tasks/:index', updateToDoTask)
router.delete('/tasks/:index', removeToDoTask)

export default router
