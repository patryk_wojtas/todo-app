import mongoose, { Schema } from 'mongoose'
import Task from '../model/TaskInterface'

const TaskSchema: Schema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  finishDate: { type: Date, required: false, default: null },
  order: { type: Number, required: true, unique: true }
})

export default mongoose.model<Task>('Task', TaskSchema)
